Source: golang-pault-go-ykpiv
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Paul Tagliamonte <paultag@debian.org>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-golang-x-crypto-dev,
               libssl-dev,
               libykpiv-dev,
               pkgconf,
               ronn,
Standards-Version: 4.5.1
Homepage: https://github.com/paultag/go-ykpiv
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-pault-go-ykpiv
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-pault-go-ykpiv.git
XS-Go-Import-Path: pault.ag/go/ykpiv
Rules-Requires-Root: no

Package: golang-pault-go-ykpiv-dev
Architecture: all
Depends: golang-golang-x-crypto-dev,
         libssl-dev,
         libykpiv-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Description: high level cgo wrapper around libykpiv.so.1
 go-ykpiv implements an idiomatic go API fit for use when applications need to
 communicate with a Yubikey in PIV mode.
 .
 Most PIV tokens, Yubikeys included, can be used as a PKCS#11 device using
 OpenSC, and Yubikeys are even capable of doing Signing and Decryption through
 that interface. However, some management functions are not exposed in the
 PKCS#11 OpenSC interface, so this library may be of use when one wants to write
 a new Certificate, or set PINs.

Package: ykls
Architecture: any
Built-Using: ${misc:Built-Using},
Depends: pcscd,
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: golang-pault-go-ykpiv-dev (<< 1.3.4-1),
Replaces: golang-pault-go-ykpiv-dev (<< 1.3.4-1),
Description: Utility for listing connected Yubikeys
 ykls is a utility for listing connected Yubikeys, along with some relevant
 information, such as enabled modes, firmware version, serial number,
 identities present in the various key slots, ...
 .
 For example:
 $ ykls
 Reader:  Yubico YubiKey FIDO+CCID 00 00
 Version: 4.3.7
 Serial:  6447364
 Slot Authentication (9a): nicoo@debian.org
